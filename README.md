# Drupal test theme in a sandbox

* https://www.drupal.org/sandbox/raffaelj/3419542
* https://git.drupalcode.org/sandbox/raffaelj-3419542

If this file exists, the initial git and ssh key setup works.

The initial setup worked, but two confusing things happened:

1. `git@git.drupal.org` vs. `https://git.drupalcode.org` - at first I thought I copied the wrong code, but cloning via ssh and cloning via https have different urls - `git remote add origin git@git.drupal.org:sandbox/raffaelj-3419542.git` is correct

2. There is no obvious place to find the ssh key fingerprint. A search engine helped: https://www.drupal.org/git-drupal-org-ssh-host-key

> Algorithm: ed25519
> MD5: b7:43:0d:44:66:9d:db:ed:d9:2c:d0:1d:52:88:62:7a
> SHA256: SHA256:dPC6RYiFfvVB/epk5s/lisF4jCbOFJnitbYPy6Dsog8

## first release

not working, because sandboxed projects don't have releases

I created a non-sandboxed test project instead: https://www.drupal.org/project/theme_test_not_sandboxed
